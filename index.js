const express = require("express");
const port = 4000;
const application = express();

application.use (express.json());
application.listen(port, () => console.log (`Welcome to our Express API Server on port: ${port}`));
application.get ('/', (request, response) => {
	response.send('Greetings! Welcome to the Express JS Introduction Activity of ROKKI GARAY');
})
const tasks = [];

application.get('/tasks', (req, res) =>{
	res.send (tasks);
});

/*application.get('/pending', (req, res) =>{
	let pending = tasks.find({status: "Pending"})
	res.send (pending);
	res.send ("These are the pending tasks that you have to do")
});*/

application.post('/task', (req,res)=> {
	console.log(req.body);
	let task = req.body;
	let taskName = req.body.name;
	let taskStatus = req.body.status;
		
	if(tasks.length !== 0){
		for (let index = 0; index < tasks.length; index++){
			if(taskName !=='' && taskStatus !=='' && taskName !== tasks[index].name)  {
			tasks.push(task);
			res.send (`A new ${taskName} task has been added to our collection`)
			break;
			} else {
				res.send('Make sure that the data is complete and is not a duplicate');
			}
		}
	} else {
		if(taskName !=='' && taskStatus !=='')  {
			tasks.push(task);
			res.send (`A new ${taskName} task has been added to our collection`)
		} else {
				res.send('Make sure that the data is complete');
		}
	}
});

application.put('/change-name', (req,res) => {
	let message;
	let target = req.body.name;
	let update = req.body.update;

	if(tasks.length !== 0){
		for (let index = 0; index < tasks.length; index++) {
			if(update === tasks[index].name){
				message = 'Duplicate already found in tasks.';
				break;
			} else {
				if (target === tasks[index].name){
				tasks[index].name = update;
				message = `A match for ${target} has been found and the task name has been updated to ${update}`;
				break;
				}
			};
		}
	} else {
		message = 'No task found';
	};

	res.send(message);
})

application.put('/change-status', (req,res) => {
	let message;
	let target = req.body.name;
	let update = req.body.updateStatus;

	if(tasks.length !== 0){
		for (let index = 0; index < tasks.length; index++) {
			if(target === tasks[index].name){
				tasks[index].status = update;
				message = `A match for ${target} has been found and the status has been updated to ${update}`;
				break;
			} else {
				message = 'No matches found!';
			};
		}
	} else {
		message = 'No task found';
	};

	res.send(message);
})

application.delete('/task',(req,res) => {
	let message;
	let delTask = req.body.name;
	if(tasks.length !== 0){
		for (let index = 0; index < tasks.length; index++) {
			if(delTask === tasks[index].name){
				tasks.splice(index, 1);
				message = `A match for ${delTask} has been found and was deleted from the resources`;
				break;
			} else {
				message = 'No matches found!';
			};
		}
	} else {
		message = 'EMPTY collection';
	};

	res.send(message);
});